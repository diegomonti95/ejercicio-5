<!DOCTYPE hmtl>
<html>
	<head>
		<title>Insertar Producto</title>
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../css/estilo.css">
		
		<?php
			include_once("../lib/funciones.php");
			$conn = fn_conectar();
		?>
	</head>
	<body>
		<table id="tabla">
			<caption>Productos</caption>
			<tr>
				<th>Nombre del Producto:</th>
				<td><input type="text" id="nombre" name="nombre" size="30" maxlength="30" value=""/></td>
			</tr>
			<tr>
				<th>Descripción:</th>
				<td><input type="text" id="descripcion" name="descripcion" size="30" maxlength="30" value=""/></td>
			</tr>
			<tr>
				<th>Marca:</th>
				<td><?php fn_listar_combo("marca","select marca_id,nombre from marca order by 2",0,"",$conn); ?></td>
			</tr>
			<tr>
				<th>Tipo:</th>
				<td><?php fn_listar_combo("tipo","select tipo_id,nombre from tipo order by 2",0,"",$conn); ?></td>
			</tr>
			<tr>
				<td colspan="2" class="centrado"><button type="button" onclick="guardar()">Guardar Cambios</button></td>
			</tr>
		</table>
		<div id="ajax-rs" class="centrado"></div>
	</body>
	<script>
		function guardar(){
			var datos={"nombre":$("#nombre").val(),
					   "descripcion":$("#descripcion").val(),
					   "marca_id":$("#marca").val(),
					   "tipo_id":$("#tipo").val()};
					   
			$.ajax({
				data: datos,
				url:  "insertar-procesar.php",
				type: "post",
				success: function(response){
					$("#ajax-rs").html(response);
				}
			});
		}
		
		$("#nombre").focus();
	</script>
	<?php
		fn_desconectar($conn);
	?>
</html>